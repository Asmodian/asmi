#include <sys/socket.h>
#include <sys/types.h>
#include <netinet/ip.h>
#include <arpa/inet.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>

int main(void)
{
    int sock;
    struct sockaddr_in client;
    int addrlen = sizeof(client);
    char buf[50];
    char *mess = (char *)(buf + 36);
    struct iphdr *ip = (struct iphdr *)buf; 
    unsigned int addr; //u_int32_t 

    if( (sock = socket( AF_INET, SOCK_RAW, IPPROTO_RAW )) < 0 )
    {
		perror("Ошибка создания сокета:");
		exit(EXIT_FAILURE);       
    }

    printf("Сервер запущен\n");

    while(1)
    {
        if( recvfrom(sock, (char *)&buf, sizeof(buf), 0, (struct sockaddr *)&client, &addrlen) < 0 )
			perror("Ошибка получения сообщения:");
        printf("Полученное сообщение: %s\n", &(*(buf+36)));
        strcpy(mess, "Hello, client");
        addr = ip->saddr;
        ip->saddr = ip->daddr;
        ip->daddr = addr;
        if( sendto(sock, (char *)buf, sizeof(buf), 0, (struct sockaddr *)&client, (socklen_t)addrlen) < 0 )
			perror("Ошибка отправки сообщения:");
    }
    exit(EXIT_SUCCESS);
}
