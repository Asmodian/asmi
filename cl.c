#include <sys/socket.h>
#include <sys/types.h>
#include <netinet/ip.h>
#include <arpa/inet.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>

int main(int argc, char* argv[])
{
    int sock;
    struct sockaddr_in server;
	int addrlen = sizeof(server);
    char buf[50];
	char *mess = (char *)(buf + 36);
    struct iphdr *ip = (struct iphdr *)buf; 

    if( (sock = socket( AF_INET, SOCK_RAW, IPPROTO_RAW )) < 0 )
    {
		perror("Ошибка создания сокета:");
		exit(EXIT_FAILURE);       
    }

	server.sin_family = AF_INET;
	memset(server.sin_zero, 0, sizeof(server.sin_zero));

	ip->ihl = 5;
	ip->version = 4;
	ip->tos = 0;
	ip->tot_len = htons(40);
	ip->frag_off = 0;
	ip->ttl = 64;
	ip->protocol = IPPROTO_RAW;
	ip->check = 0;
	inet_pton(AF_INET, argv[1], (struct in_addr *)&ip->saddr); //ip источника
	inet_pton(AF_INET, argv[2], (struct in_addr *)&ip->daddr); //ip приемника

	strcpy(mess, "Hello, server");
	
    if( sendto(sock, (char *)buf, sizeof(buf), 0, (struct sockaddr *)&server, (socklen_t)addrlen) < 0 )
		perror("Ошибка отправки сообщения:");
	if( recvfrom(sock, (char *)&buf, sizeof(buf), 0, (struct sockaddr *)&server, &addrlen) < 0 )
		perror("Ошибка получения сообщения:");
	printf("Полученное сообщение: %s\n", &(*(buf+36)));
	exit(EXIT_SUCCESS);
}
